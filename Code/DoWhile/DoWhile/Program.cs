﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoWhile
{
    class Program
    {
        static void Main(string[] args)
        {
            var i = int.Parse(Console.ReadLine());
            var counter = 20;

            do
            {
                var a = i + 1;
                Console.WriteLine($"This is line number {a}");

                i++;

            } while (i < counter);
            
        }
    }
}
