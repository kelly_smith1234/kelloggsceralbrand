﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loop
{
    class Program
    {
        static void Main(string[] args)
        {
            var count = 5;
            var i = 0;

            for (i = 0; i < count; i++)
            {
                var a = i + 1;
                Console.WriteLine($"This is line number {a}");
            }
        }
    }
}
