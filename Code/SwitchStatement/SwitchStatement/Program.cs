﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwitchStatement
{
    class Program
    {
        static void Main(string[] args)
        {
            var colour = Console.ReadLine();


            switch (colour)
            {
                case "red":
                    Console.WriteLine("You chose blue - The sky is blue");
                    break;

                case "blue":
                    Console.WriteLine("You chose red - apples are red");
                    break;

                default:
                    Console.WriteLine("You chose the wrong colour");
                    break;


            }

        }
    }
}
