﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwitchStatement2
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Console.WriteLine("What would you like to do? (km2mile / mile2km)");

            var measure = Console.ReadLine();

            switch (measure)
            {
                case "km2mile":
                    double inputkm = 0;
                    double mile = 0.621371;

                    Console.WriteLine("how many kilometres?");
                    inputkm = double.Parse(Console.ReadLine());

                    Console.WriteLine($"{inputkm}km is equal to {inputkm* mile} miles");
                    break;

                case "mile2km":
                    double inputmile = 0;
                    double km = 1.609344;
                    Console.WriteLine("how many miles?");
                    inputmile = double.Parse(Console.ReadLine());

                    Console.WriteLine($"{inputmile}miles is equal to {inputmile * km} miles");
                    break;

                default:
                    Console.WriteLine("That is not a valid input");
                    break;
            }

        }
    }
}
